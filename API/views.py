from django.utils.decorators import method_decorator
from rest_framework import viewsets, filters, generics

from drf_yasg.utils import swagger_auto_schema

from permissions import *
from utils import StandardResultsSetPagination
from API.serializers import *


# ViewSet for MyUser model
@method_decorator(name='list', decorator=swagger_auto_schema(
    manual_parameters=[],
))
class MyUserViewSet(viewsets.ModelViewSet):
    queryset = MyUser.objects.all()
    serializer_class = ListMyUserSerializer
    permission_classes = (permissions.IsAuthenticated, IsMeOrReadOnly)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    pagination_class = StandardResultsSetPagination
    search_fields = ('user__username', 'user__last_name', 'user__first_name', 'user__email')
    ordering_fields = ('user__username', 'user__last_name', 'user__first_name', 'user__email', 'user__date_joined')

    def get_serializer_class(self):
        if self.action == 'list':
            return ListMyUserSerializer
        else:
            return DetailMyUserSerializer

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        return queryset


# Custom view to get current MyUser
class CurrentMyUserView(generics.RetrieveUpdateAPIView):
    serializer_class = DetailMyUserSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        myuser = MyUser.get_myuser_with_user(user=self.request.user)
        self.check_object_permissions(self.request, myuser)
        return myuser


# ViewSet for Theme model
class ThemeViewSet(viewsets.ModelViewSet):
    queryset = Theme.objects.all()
    serializer_class = ThemeSerializer
    permission_classes = (IsAdminOrReadOnly,)


# ViewSet for Universe model
class UniverseViewSet(viewsets.ModelViewSet):
    queryset = Universe.objects.all()
    serializer_class = UniverseSerializer
    permission_classes = (IsAdminOrReadOnly,)


# ViewSet for Genre model
class GenreViewSet(viewsets.ModelViewSet):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer
    permission_classes = (IsAdminOrReadOnly,)


# ViewSet for Editor model
class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = (IsAdminOrReadOnly,)


# ViewSet for Personality model
class PersonalityViewSet(viewsets.ModelViewSet):
    queryset = Personality.objects.all()
    serializer_class = ListPersonalitySerializer
    permission_classes = (IsAdminOrReadOnly,)

    def get_serializer_class(self):
        if self.action == 'list':
            return ListPersonalitySerializer
        else:
            return DetailPersonalitySerializer

#
# # ViewSet for Developer model
# class DeveloperViewSet(viewsets.ModelViewSet):
#     queryset = Developer.objects.all()
#     serializer_class = DeveloperSerializer
#     permission_classes = (IsAdminOrReadOnly,)
#
#
# # ViewSet for Platform model
# class PlatformViewSet(viewsets.ModelViewSet):
#     queryset = Platform.objects.all()
#     serializer_class = PlatformSerializer
#     permission_classes = (IsAdminOrReadOnly,)


# ViewSet for Work model
class WorkViewSet(viewsets.ModelViewSet):
    queryset = Work.objects.filter(available=True)
    serializer_class = ListWorkSerializer
    permission_classes = (IsAdminOrReadOnly,)

    def get_serializer_class(self):
        if self.action == 'list':
            return ListWorkSerializer
        else:
            instance = self.get_object()
            if isinstance(instance, VideoGame):
                return DetailVideoGameSerializer
            if isinstance(instance, Book):
                return DetailBookSerializer
            if isinstance(instance, ComicBook):
                return DetailComicBookSerializer
            if isinstance(instance, Movie):
                return DetailMovieSerializer
            if isinstance(instance, Series):
                return DetailSeriesSerializer
            return DetailWorkSerializer

    def get_object(self):
        instance = Work.get_work_with_id(self.kwargs["pk"])
        return instance


# View to create AddRequest instance
class CreateAddRequestView(generics.CreateAPIView):
    queryset = AddRequest.objects.all()
    serializer_class = AddRequestSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        myuser = MyUser.get_myuser_with_user(self.request.user)
        serializer.save(creator=myuser)


# View to list AddRequest instances for current user
class ListCurrentAddRequestView(generics.ListAPIView):
    queryset = AddRequest.objects.all()
    serializer_class = AddRequestSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        myuser = MyUser.get_myuser_with_user(self.request.user)
        return self.queryset.filter(creator=myuser)
