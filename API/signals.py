from django.db.models.signals import pre_save, post_save

from django.dispatch import receiver

from utils import link_related_works
from API.models import *


# Signal to create MyUser instance when User instance is created
@receiver(post_save, sender=User)
def registration(sender, instance, created, **kwargs):
    if created:
        MyUser.objects.create(user=instance)


@receiver(post_save, sender=VideoGame)
def link_related_at_save(sender, instance, created, **kwargs):
    if created:
        link_related_works(instance)
