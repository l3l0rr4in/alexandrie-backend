from django.contrib import admin

from API.models import *

# Register your models here.


admin.site.register(MyUser)
admin.site.register(AddRequest)

# Themes models
admin.site.register(Theme)
admin.site.register(Universe)
admin.site.register(Genre)

# Works relative Models
admin.site.register(Company)
admin.site.register(Personality)

# Video Games Models
admin.site.register(Platform)
admin.site.register(Developer)
admin.site.register(GameGenre)
admin.site.register(VideoGame)

# Books Models
admin.site.register(Book)

# ComicBooks Models
admin.site.register(ComicGenre)
admin.site.register(ComicBook)

# Movies Models
admin.site.register(Movie)

# Series Models
admin.site.register(Series)
