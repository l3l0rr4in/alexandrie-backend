from rest_framework import serializers

from API.models import *


# Custom serializer for MyUser model (List case)
class ListMyUserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True, source="user.username")
    first_name = serializers.CharField(read_only=True, source="user.first_name")
    last_name = serializers.CharField(read_only=True, source="user.last_name")
    email = serializers.CharField(read_only=True, source="user.email")
    is_staff = serializers.BooleanField(read_only=True, source="user.is_staff")
    is_active = serializers.BooleanField(read_only=True, source="user.is_active")
    date_joined = serializers.DateTimeField(read_only=True, source="user.date_joined")

    class Meta:
        model = MyUser
        fields = "__all__"


# Custom serializer for MyUser model (Detail case)
class DetailMyUserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(read_only=True, source="user.username")
    first_name = serializers.CharField(read_only=True, source="user.first_name")
    last_name = serializers.CharField(read_only=True, source="user.last_name")
    email = serializers.CharField(read_only=True, source="user.email")
    is_staff = serializers.BooleanField(read_only=True, source="user.is_staff")
    is_active = serializers.BooleanField(read_only=True, source="user.is_active")
    date_joined = serializers.DateTimeField(read_only=True, source="user.date_joined")

    class Meta:
        model = MyUser
        fields = "__all__"


class ThemeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Theme
        fields = "__all__"


class UniverseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Universe
        fields = "__all__"


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = "__all__"


class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media
        fields = "__all__"


class ListWorkSerializer(serializers.ModelSerializer):
    themes = ThemeSerializer(many=True, read_only=True)
    universe = UniverseSerializer(many=True, read_only=True)
    genres = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = Work
        fields = "__all__"


class DetailWorkSerializer(serializers.ModelSerializer):
    themes = ThemeSerializer(many=True, read_only=True)
    universe = UniverseSerializer(many=True, read_only=True)
    genres = GenreSerializer(many=True, read_only=True)
    work_medias = MediaSerializer(many=True, read_only=True)
    related_works = serializers.SerializerMethodField()

    class Meta:
        model = Work
        fields = "__all__"

    def get_related_works(self, instance):
        res_tab = []
        for elem in instance.related_works.all():
            instance = Work.get_work_with_id(elem.pk)
            if isinstance(instance, VideoGame):
                res_tab.append(ListVideoGameSerializer(instance, many=False).data)
        return res_tab


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = "__all__"


class DeveloperSerializer(serializers.ModelSerializer):
    class Meta:
        model = Developer
        fields = "__all__"


class PlatformSerializer(serializers.ModelSerializer):
    class Meta:
        model = Platform
        fields = "__all__"


class ListPersonalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Personality
        fields = "__all__"


class DetailPersonalitySerializer(serializers.ModelSerializer):

    # Add all work contributions

    class Meta:
        model = Personality
        fields = "__all__"


class ListVideoGameSerializer(ListWorkSerializer):
    editor = CompanySerializer(many=False, read_only=True)
    platforms = PlatformSerializer(many=True, read_only=True)
    game_genres = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = VideoGame
        fields = "__all__"


class DetailVideoGameSerializer(DetailWorkSerializer):
    editor = CompanySerializer(many=False, read_only=True)
    developers = DeveloperSerializer(many=True, read_only=True)
    directors = ListPersonalitySerializer(many=True, read_only=True)
    producers = ListPersonalitySerializer(many=True, read_only=True)
    programmers = ListPersonalitySerializer(many=True, read_only=True)
    compositors = ListPersonalitySerializer(many=True, read_only=True)
    platforms = PlatformSerializer(many=True, read_only=True)
    game_genres = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = VideoGame
        fields = "__all__"


class ListBookSerializer(ListWorkSerializer):
    authors = ListPersonalitySerializer(many=True, read_only=True)
    editors = CompanySerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = "__all__"


class DetailBookSerializer(DetailWorkSerializer):
    authors = ListPersonalitySerializer(many=True, read_only=True)
    editors = CompanySerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = "__all__"


class ListComicBookSerializer(ListWorkSerializer):
    authors = ListPersonalitySerializer(many=True, read_only=True)
    illustrators = ListPersonalitySerializer(many=True, read_only=True)
    comic_genres = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = ComicBook
        fields = "__all__"


class DetailComicBookSerializer(DetailWorkSerializer):
    authors = ListPersonalitySerializer(many=True, read_only=True)
    illustrators = ListPersonalitySerializer(many=True, read_only=True)
    editors = CompanySerializer(many=True, read_only=True)
    comic_genres = GenreSerializer(many=True, read_only=True)

    class Meta:
        model = ComicBook
        fields = "__all__"


class ListMovieSerializer(ListWorkSerializer):
    directors = ListPersonalitySerializer(many=True, read_only=True)
    production_company = CompanySerializer(many=True, read_only=True)
    distributor = CompanySerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        fields = "__all__"


class DetailMovieSerializer(DetailWorkSerializer):
    directors = ListPersonalitySerializer(many=True, read_only=True)
    producers = ListPersonalitySerializer(many=True, read_only=True)
    script_writers = ListPersonalitySerializer(many=True, read_only=True)
    compositors = ListPersonalitySerializer(many=True, read_only=True)
    production_company = CompanySerializer(many=True, read_only=True)
    distributor = CompanySerializer(many=True, read_only=True)
    editors = CompanySerializer(many=True, read_only=True)

    class Meta:
        model = Movie
        fields = "__all__"


class ListSeriesSerializer(ListWorkSerializer):
    executive_producers = ListPersonalitySerializer(many=True, read_only=True)
    producers = ListPersonalitySerializer(many=True, read_only=True)
    production_company = CompanySerializer(many=True, read_only=True)
    broadcaster = CompanySerializer(many=False, read_only=True)

    class Meta:
        model = Series
        fields = "__all__"


class DetailSeriesSerializer(DetailWorkSerializer):
    executive_producers = ListPersonalitySerializer(many=True, read_only=True)
    producers = ListPersonalitySerializer(many=True, read_only=True)
    directors = ListPersonalitySerializer(many=True, read_only=True)
    script_writers = ListPersonalitySerializer(many=True, read_only=True)
    compositors = ListPersonalitySerializer(many=True, read_only=True)
    production_company = CompanySerializer(many=True, read_only=True)
    distributor = CompanySerializer(many=True, read_only=True)
    editors = CompanySerializer(many=True, read_only=True)

    class Meta:
        model = Series
        fields = "__all__"


class AddRequestSerializer(serializers.ModelSerializer):
    creator = DetailMyUserSerializer(read_only=True)
    date_request = serializers.DateTimeField(read_only=True)
    status = serializers.CharField(read_only=True)

    class Meta:
        model = AddRequest
        fields = "__all__"
