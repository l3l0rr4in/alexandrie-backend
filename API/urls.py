from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from API.views import *

router = DefaultRouter()
router.register(r'users', MyUserViewSet)
router.register(r'themes', ThemeViewSet)
router.register(r'universes', UniverseViewSet)
router.register(r'genres', GenreViewSet)
router.register(r'companies', CompanyViewSet)
router.register(r'personalities', PersonalityViewSet)
router.register(r'works', WorkViewSet)

urlpatterns = [
    url(r'^user/me/$', CurrentMyUserView.as_view()),
    url(r'^add_requests/me/$', ListCurrentAddRequestView.as_view()),

    url(r'^add_request/submit/$', CreateAddRequestView.as_view()),

    url(r'^', include(router.urls)),
]
