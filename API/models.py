from django.db import models
from django.http import Http404
from django.utils import timezone
from django.contrib.auth.models import User

from jsonfield import JSONField

from validators import validate_image_extension, validate_media_extension


class MyUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    avatar = models.ImageField(upload_to="users/avatars/", null=True, blank=True, validators=[validate_image_extension])
    appreciate_works = models.ManyToManyField("Work", related_name="loved_by", blank=True)

    # static method used to get MyUser instance with a User instance or return 404 if not found
    @staticmethod
    def get_myuser_with_user(user):
        try:
            myuser = MyUser.objects.get(user=user)
        except(MyUser.DoesNotExist):
            raise Http404("There is no matching MyUser")
        else:
            return myuser


class Instance(models.Model):
    name = models.CharField(max_length=256, null=True)
    image = models.ImageField(upload_to="instances/", null=True, blank=True,
                              validators=[validate_image_extension])
    description = models.TextField(max_length=4096, null=True)
    link = models.URLField(null=True)

    class Meta:
        abstract = True

    def __str__(self):
        if self.name is not None:
            return self.name
        else:
            return "-"


class Theme(Instance):
    pass


class Universe(Instance):
    pass


class Genre(Instance):
    pass


class Company(Instance):
    pass


class Personality(Instance):
    pass


class Work(models.Model):
    available = models.BooleanField(default=False)
    name = models.CharField(max_length=256, null=True)
    image = models.ImageField(upload_to="works/", null=True, blank=True, validators=[validate_image_extension])
    release_date = models.DateField(null=True)
    description = models.TextField(max_length=4096, null=True)
    link = models.URLField(null=True)
    related_works = models.ManyToManyField("self", symmetrical=True, blank=True, through="Relation")
    themes = models.ManyToManyField(Theme, related_name="works_associated")
    universe = models.ManyToManyField(Universe, related_name="works_associated")
    genres = models.ManyToManyField(Genre, related_name="works_associated")

    def __str__(self):
        if self.name is not None:
            return self.name
        else:
            return "Not named Work"

    @staticmethod
    def get_work_with_id(pk):
        for work_type in WORKS_MODELS:
            try:
                instance = work_type.objects.get(pk=pk)
                return instance
            except(work_type.DoesNotExist):
                pass
        return None


class Relation(models.Model):
    work_1 = models.ForeignKey(Work, on_delete=models.CASCADE, null=True, related_name="work_relations_right")
    work_2 = models.ForeignKey(Work, on_delete=models.CASCADE, null=True, related_name="work_relations_left")
    relation_weight = models.FloatField(default=0.0)


class Media(models.Model):
    media = models.FileField(upload_to="works/", null=True, blank=True, validators=[validate_media_extension])
    work = models.ForeignKey(Work, on_delete=models.CASCADE, null=True, related_name="work_medias")


class Developer(Instance):
    pass


class Platform(Instance):
    release_date = models.DateField(null=True)
    editor = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, related_name="edited_platforms")


class GameGenre(Genre):
    pass


class VideoGame(Work):

    MODE_CHOICE = (
        ("-", "-",),
        ("Solo", "Solo",),
        ("Two Player", "Two Player",),
        ("MultiPlayer", "MultiPlayer",),
        ("Massive MultiPlayer", "Massive MultiPlayer"),
    )

    editor = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True, related_name="edited_games")
    developers = models.ManyToManyField(Developer, blank=True)
    directors = models.ManyToManyField(Personality, related_name="game_director", blank=True)
    producers = models.ManyToManyField(Personality, related_name="game_producer", blank=True)
    programmers = models.ManyToManyField(Personality, related_name="game_programmer", blank=True)
    compositors = models.ManyToManyField(Personality, related_name="game_compositor", blank=True)
    platforms = models.ManyToManyField(Platform, blank=True)
    game_genres = models.ManyToManyField(GameGenre, blank=True)
    mode_1 = models.CharField(max_length=128, choices=MODE_CHOICE, default="Solo")
    mode_2 = models.CharField(max_length=128, choices=MODE_CHOICE, default="-")


class Book(Work):
    authors = models.ManyToManyField(Personality, related_name="book_author", blank=True)
    editors = models.ManyToManyField(Company, blank=True)


class Movie(Work):
    directors = models.ManyToManyField(Personality, related_name="movie_director", blank=True)
    producers = models.ManyToManyField(Personality, related_name="movie_producer", blank=True)
    script_writers = models.ManyToManyField(Personality, related_name="movie_script_writer", blank=True)
    compositors = models.ManyToManyField(Personality, related_name="movie_compositor", blank=True)
    production_company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True,
                                           related_name="produced_movies")
    distributor = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True,
                                    related_name="distributed_movies")
    editors = models.ManyToManyField(Company, blank=True)


class Series(Work):
    executive_producers = models.ManyToManyField(Personality, related_name="series_executive_producer", blank=True)
    producers = models.ManyToManyField(Personality, related_name="series_producer", blank=True)
    compositors = models.ManyToManyField(Personality, related_name="series_compositor", blank=True)
    directors = models.ManyToManyField(Personality, related_name="series_director")
    script_writers = models.ManyToManyField(Personality, related_name="series_script_writer", blank=True)
    production_company = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True,
                                           related_name="produced_series")
    distributor = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True,
                                    related_name="distributed_series")
    broadcaster = models.ForeignKey(Company, on_delete=models.SET_NULL, null=True,
                                    related_name="broadcast_series")
    editors = models.ManyToManyField(Company, blank=True)

    nb_of_seasons = models.PositiveIntegerField(default=1)
    nb_of_episodes = models.PositiveIntegerField(default=1)


class ComicGenre(Genre):
    pass


class ComicBook(Work):

    FREQUENCY_CHOICE = (
        ("-", "-",),
        ("Weekly", "Weekly",),
        ("Bimonthly", "Bimonthly",),
        ("Monthly", "Monthly",)
    )

    authors = models.ManyToManyField(Personality, related_name="comic_author", blank=True)
    illustrators = models.ManyToManyField(Personality, related_name="comic_illustrator", blank=True)
    editors = models.ManyToManyField(Company, blank=True)
    comic_genres = models.ManyToManyField(ComicGenre, blank=True)

    publication_frequency = models.CharField(max_length=128, choices=FREQUENCY_CHOICE, default="Weekly")
    nb_of_chapters = models.PositiveIntegerField(default=1)
    nb_of_volumes = models.PositiveIntegerField(default=1)


WORKS_MODELS = [VideoGame, Movie, Series, Book, ComicBook]


class AddRequest(models.Model):

    STATUS_CHOICE = (
        ('PENDING', 'PENDING'),
        ('VALIDATED', 'VALIDATED'),
        ('REFUSED', 'REFUSED'),
    )

    creator = models.ForeignKey(MyUser, on_delete=models.SET_NULL, null=True, related_name="add_request_sending")
    date_request = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=24, default="PENDING", choices=STATUS_CHOICE)
    json = JSONField()

    def __str__(self):
        if self.creator is not None and self.creator.user is not None and self.creator.user.username is not None:
            return "%s %s" % (self.creator.user.username, self.date_request)
        else:
            return "-"
