from rest_framework import permissions


class IsAdminOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view,):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.method in permissions.SAFE_METHODS or request.user.is_staff:
            return True

        else:
            return False


# Permission on object if current user is object.user or is staff
class IsMeOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        # If not staff, can't list instances or create a new instance
        elif request.user.is_staff is False and (view.action == 'list' or view.action == 'create'):
            return False
        else:
            return True

    def has_object_permission(self, request, view, obj):
        if request.user.is_staff:
            return True

        return obj.user == request.user
