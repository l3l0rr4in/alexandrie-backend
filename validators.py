import os
from django.conf import settings
from django.core.exceptions import ValidationError


# Custom validator function to check image extension
def validate_image_extension(value):
    ext = os.path.splitext(value.name)[1]
    if not ext.lower() in settings.IMAGE_EXT:
        raise ValidationError('Unsupported file extension.')


# Custom validator function to check media file extension
def validate_media_extension(value):
    ext = os.path.splitext(value.name)[1]
    if not ext.lower() in settings.MEDIA_EXT:
        raise ValidationError('Unsupported file extension.')


# Custom validator function to check doc file extension
def validate_doc_extension(value):
    ext = os.path.splitext(value.name)[1]
    if not ext.lower() in settings.DOC_EXT:
        raise ValidationError('Unsupported file extension.')
