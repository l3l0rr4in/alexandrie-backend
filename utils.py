from rest_framework import pagination

from API.models import *


# Custom Paginator class for viewsets
class StandardResultsSetPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


# fct to link related works to current Work instance
def link_related_works(instance):
    # try:
    #     qs = Work.objects.none()
    #     # Link related Work with common fields
    #     qs_same_themes = Work.objects.filter(themes__in=instance.themes.all()).exclude(pk=instance.pk)
    #     qs_same_universes = Work.objects.filter(universe__in=instance.universe.all()).exclude(pk=instance.pk)
    #
    #     # qs_same_genres = VideoGame.objects.filter(genres__in=instance.genres.all()).exclude(pk=instance.pk)
    #     qs = qs.union(qs_same_themes, qs_same_universes)
    #     for elem in qs:
    #         Relation.objects.create(work_1=instance, work_2=elem, relation_weight=calc_weight(instance, elem))
    # except(ValueError):
    #     pass
    pass


# # fct to calculate weight of relation between 2 instance
# def calc_weight(instance_1, instance_2):
#     base_weight = 2.0
#
#     return 0.0
